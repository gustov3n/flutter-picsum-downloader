import 'dart:typed_data';

import 'package:flutter/material.dart';
import '../constants.dart';
import '../widgets/circular_progress_small.dart';
import '../widgets/form_numeric.dart';
import '../picsum_download.dart';
import '../widgets/image_tiles.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _isLoading = false;
  final TextEditingController _conCount = TextEditingController(text: '1');
  final TextEditingController _conWidth = TextEditingController(text: '200');
  final TextEditingController _conHeight = TextEditingController(text: '300');

  final List<Uint8List> _images = [];

  int get _getCount => int.tryParse(_conCount.text) ?? 0;
  int get _getWidth => int.tryParse(_conWidth.text) ?? 0;
  int get _getHeight => int.tryParse(_conHeight.text) ?? 0;

  void _getImages() async {
    setState(() {
      _isLoading = true;
      _images.clear();
    });

    int count = _getCount;
    int width = _getWidth;
    int height = _getHeight;

    if (count > 0 && width > 0 && height > 0) {
      for (int i = 0; i < count; i++) {
        var imageBytes = await picsumDownload(width: width, height: height);
        if (imageBytes == null) {
          break;
        }
        _images.add(imageBytes);
      }
    }

    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FormNumeric(
              controller: _conCount,
              label: 'Images Count',
            ),
            const SizedBox(height: 16),
            FormNumeric(
              controller: _conWidth,
              label: 'Width (px)',
            ),
            FormNumeric(
              controller: _conHeight,
              label: 'Height (px)',
            ),
            const SizedBox(height: 16),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: _isLoading ? null : _getImages,
                child: _isLoading
                    ? const CircularProgressSmall()
                    : const Text('Get Images!'),
              ),
            ),
            Expanded(
              child: ImageTiles(
                imageWidth: _getWidth,
                imageHeight: _getHeight,
                images: _images,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
