import 'dart:io';
import 'dart:typed_data';

import 'package:http/http.dart' as http;

Future<Uint8List?> picsumDownload({
  int width = 200,
  int height = 300,
}) async {
  var url = Uri.parse('https://picsum.photos/$width/$height');

  try {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      return response.bodyBytes;
    }
  } catch (e) {
    stdout.writeln('I\'m sorry an error has occured ;(');
  }

  return null;
}
