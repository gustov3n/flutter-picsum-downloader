import 'package:flutter/material.dart';

class FormNumeric extends StatelessWidget {
  final TextEditingController? controller;
  final String label;

  const FormNumeric({
    this.controller,
    this.label = '',
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        label: Text(label),
      ),
    );
  }
}
