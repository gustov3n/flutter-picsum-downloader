import 'package:flutter/material.dart';

class CircularProgressSmall extends StatelessWidget {
  const CircularProgressSmall({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      width: 24,
      height: 24,
      child: CircularProgressIndicator(),
    );
  }
}
