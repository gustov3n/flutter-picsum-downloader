import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageTiles extends StatelessWidget {
  final int imageWidth;
  final int imageHeight;
  final List<Uint8List> images;

  const ImageTiles({
    required this.imageWidth,
    required this.imageHeight,
    required this.images,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SizedBox(
        width: double.infinity,
        child: Wrap(
          alignment: WrapAlignment.center,
          children: images
              .map((e) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.memory(
                      e,
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}
